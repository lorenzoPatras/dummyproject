class AutomationResult {
  String status;
  String url;

  public AutomationResult(String status, String url) {
    this.status = status;
    this.url = url;
  }
}

def dummyFunction(param) {
  println("param from other file " + param)

  return new AutomationResult("first", "second");
}

return this

import unittest
import xmlrunner

from test_math import *


class TestDummy(unittest.TestCase):
    def setUp(self):
        self.seq = list(range(10))

    @unittest.skip("demonstrating skipping")
    def test_skipped(self):
        self.fail("shouldn't happen")

    def test_sample(self):
        self.assertIn(5, self.seq)

    # @unittest.skip("skip failed test")
    def test_failed(self):
        self.assertTrue(False)

if __name__ == '__main__':
    with open('test_report.xml', 'wb') as output:
        unittest.main(
            testRunner=xmlrunner.XMLTestRunner(output=output),
            failfast=False, buffer=False, catchbreak=False)

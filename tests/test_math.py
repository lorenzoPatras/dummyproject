import unittest

from src.math_util import simple_algebra as al

class TestMathFunctions(unittest.TestCase):
    def test_add(self):
        self.assertEqual(al.add(2, 2), 4)

    def test_subtract(self):
        self.assertEqual(al.subtract(5, 3), 2)

    def test_multiply(self):
        self.assertEqual(al.multiply(3, 2), 6)

    def test_divide(self):
        self.assertEqual(al.divide(4, 2), 2)
